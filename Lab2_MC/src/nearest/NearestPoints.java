package nearest;

public interface NearestPoints 
{
	public final static int NO_VALUE = -1;
	
	public void solveNearest();
	
	public void solveNearestExhaustive();
	
	public void sort();
	
	public int getSmallestIndexLeft();

	public int getSmallestIndexRight();

	public double getDistance();
}
