package nearest;

import java.awt.Point;
import java.util.Collections;
import java.util.Comparator;
import java.util.Vector;
/**
 * made by Michael Chen
 */
public class NearestPointsConcrete implements NearestPoints
{
	private Vector<Point> PointsVector; // vector used for storing points
	private double shortestDist; // holds shortest distance
	private int smallIndexLeft; // holds left index number of shortest distance
	private int smallIndexRight; // holds right index number of shortest distance
	public NearestPointsConcrete()
	{
		PointsVector=new Vector<Point>();
		shortestDist=Double.MAX_VALUE;
		smallIndexLeft=NO_VALUE;
		smallIndexRight=NO_VALUE;
	}
	
	/**
	 * This method should take the Vector<Point> of points added
	 * in this class and find the two points that are nearest.  
	 * The results should be accessed using the getters provided
	 * below.  This method MUST be recursive and O(nlg(n)) in 
	 * complexity using the method we discussed in class.
	 */
	/**
	 * @param the left most point and the right most point
	 * method calculates shortest distance and their index
	 */
	private void solvingNearest(int left, int right)
	{
		 
		if(right>left)
		{
			int middle=(right-left)/2+left;
			Point point1=(Point)PointsVector.get(left);
				double x1=(Double)point1.getX();
				double y1=(Double)point1.getY();
			Point point2=(Point)PointsVector.get(right);
				double x2=(Double)point2.getX();
				double y2=(Double)point2.getY();
			double dist=Math.sqrt(Math.pow(x1-x2,2)+ Math.pow(y1-y2,2));
			if(dist<shortestDist)
			{
				shortestDist=dist;
				smallIndexLeft=left;
				smallIndexRight=right;
			}
			if(middle>left)
			{
				solvingNearest(middle,right);
			}
			if(middle<right)
			{
				solvingNearest(left,middle);
			}
		}

	}
	public void solveNearest()
	{
		sort(); 
		solvingNearest(0,PointsVector.size()-1); //calls above method
	}
	
	/**
	 * This method should take the Vector<Point> of points added
	 * in this class and find the two points that are nearest.  
	 * The results should be accessed using the getters provided
	 * below.  This method is NOT recursive and O(n^2) in 
	 * complexity, as we discussed in class, by taking the first
	 * point and comparing the distance to every other point, then
	 * moving to the next point and so on.
	 */
	public void solveNearestExhaustive()
	{
		for(int i=0;i<PointsVector.size();i++)
		{
			for(int g=0;g<PointsVector.size();g++) // O(n^2) Exhaustive loop
			{
				if(i!=g)
				{
					Point point1=(Point)PointsVector.get(i);
						double x1=(Double)point1.getX();
						double y1=(Double)point1.getY();
					Point point2=(Point)PointsVector.get(g);
						double x2=(Double)point2.getX();
						double y2=(Double)point2.getY();
					double dist=Math.sqrt(Math.pow(x1-x2,2)+ Math.pow(y1-y2,2));
					if(dist<shortestDist)
					{
						shortestDist=dist;
						smallIndexLeft=i;
						smallIndexRight=g;
					}
				}
			}
		}
	}
	
	public void add(Point point)
	{
		PointsVector.add(point);
	}
	public void sort()
	{
		// library that is used for sorting points with x value
		Collections.sort(PointsVector, new Comparator<Point>()
		{
			public int compare(Point p1, Point p2)
			{
				return Double.compare((p1.getX()),p2.getX());
			}
		});
	}
	
	public int getSmallestIndexLeft()
	{
		return smallIndexLeft;
	}

	public int getSmallestIndexRight()
	{
		return smallIndexRight;
	}

	public double getDistance()
	{
		return shortestDist;
	}
}
