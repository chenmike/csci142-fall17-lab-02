package nearesttests;

import static org.junit.Assert.*;

import java.awt.Point;

import org.junit.Before;
import org.junit.Test;

import nearest.NearestPointsConcrete;

public class TestNearest {
	private NearestPointsConcrete myNearestPointsConcrete;
	private double myError;
	
	@Before
	public void setUp() throws Exception {
		myNearestPointsConcrete = new NearestPointsConcrete();
		myError = 0.00001;
	}

	@Test
	public void testOnePoint() {
		myNearestPointsConcrete.add(new Point(5, 4));
		myNearestPointsConcrete.solveNearest();
		assertEquals(NearestPointsConcrete.NO_VALUE, myNearestPointsConcrete.getSmallestIndexLeft());
		assertEquals(NearestPointsConcrete.NO_VALUE, myNearestPointsConcrete.getSmallestIndexRight());
		assertEquals(Double.MAX_VALUE, myNearestPointsConcrete.getDistance(), myError);
	}
	
	@Test
	public void testTwoPointsNegs() {
		myNearestPointsConcrete.add(new Point(3, -4));
		myNearestPointsConcrete.add(new Point(-2, 2));
		myNearestPointsConcrete.solveNearest();
		assertEquals(0, myNearestPointsConcrete.getSmallestIndexLeft());
		assertEquals(1, myNearestPointsConcrete.getSmallestIndexRight());
		assertEquals(7.81025, myNearestPointsConcrete.getDistance(), myError);
	}

	@Test
	public void testThreePoints() {
		myNearestPointsConcrete.add(new Point(5, 4));
		myNearestPointsConcrete.add(new Point(4, 4));
		myNearestPointsConcrete.add(new Point(3, 5));
		myNearestPointsConcrete.solveNearest();
		assertEquals(1.0, myNearestPointsConcrete.getDistance(), myError);
	}
	@Test
	public void isoscelesTriangle() {
		myNearestPointsConcrete.add(new Point(3, 5));
		myNearestPointsConcrete.add(new Point(2, 0));
		myNearestPointsConcrete.add(new Point(4, 0));
		myNearestPointsConcrete.solveNearest();
		assertEquals(2.0, myNearestPointsConcrete.getDistance(), myError);
	}
	@Test
	public void scatteredPoints() {
		myNearestPointsConcrete.add(new Point(4, 4));
		myNearestPointsConcrete.add(new Point(5, 8));
		myNearestPointsConcrete.add(new Point(7, 4));
		myNearestPointsConcrete.add(new Point(10, 6));
		myNearestPointsConcrete.add(new Point(8, 5));
		myNearestPointsConcrete.add(new Point(2, 10));
		myNearestPointsConcrete.solveNearest();
		assertEquals(3, myNearestPointsConcrete.getSmallestIndexLeft());
		assertEquals(4, myNearestPointsConcrete.getSmallestIndexRight());
		assertEquals(1.41421, myNearestPointsConcrete.getDistance(), myError);
	}
	@Test
	public void BigQuadrilateral() {
		myNearestPointsConcrete.add(new Point(100, 100));
		myNearestPointsConcrete.add(new Point(180, 20));
		myNearestPointsConcrete.add(new Point(0, 0));
		myNearestPointsConcrete.add(new Point(80, 40));
		myNearestPointsConcrete.solveNearest();
		assertEquals(1, myNearestPointsConcrete.getSmallestIndexLeft());
		assertEquals(2, myNearestPointsConcrete.getSmallestIndexRight());
		assertEquals(63.24555, myNearestPointsConcrete.getDistance(), myError);
	}
	@Test
	public void PointsWithSameX() {
		myNearestPointsConcrete.add(new Point(1000, 1000));
		myNearestPointsConcrete.add(new Point(1000, 900));
		myNearestPointsConcrete.add(new Point(550, 700));
		myNearestPointsConcrete.add(new Point(550, 40));
		myNearestPointsConcrete.add(new Point(120, 400));
		myNearestPointsConcrete.add(new Point(120, 650));
		myNearestPointsConcrete.solveNearest();
		assertEquals(4, myNearestPointsConcrete.getSmallestIndexLeft());
		assertEquals(5, myNearestPointsConcrete.getSmallestIndexRight());
		assertEquals(100, myNearestPointsConcrete.getDistance(), myError);
	}
	@Test
	public void ClosePoints() {
		myNearestPointsConcrete.add(new Point(10, 20));
		myNearestPointsConcrete.add(new Point(50, 20));
		myNearestPointsConcrete.add(new Point(30, 0));
		myNearestPointsConcrete.add(new Point(30, 40));
		myNearestPointsConcrete.add(new Point(30, 20));
		myNearestPointsConcrete.solveNearest();
		assertEquals(3, myNearestPointsConcrete.getSmallestIndexLeft());
		assertEquals(4, myNearestPointsConcrete.getSmallestIndexRight());
		assertEquals(20, myNearestPointsConcrete.getDistance(), myError);
	}
	
}
